//2019 2b
#include <iostream>
using namespace std;

//class creation
class Time{
  public:
  int hour,minute,second;

  void inputTime(string times){
    cout<<"Input "+times+ " hour minute and seconds \n\n" ;
    cout<<"Hour:" ;
    cin>>hour;
    cout<<"Minute:" ;
    cin>>minute;
    cout<<"Second:" ;
    cin>>second;
  }

};

void addTwoTimes(){
  Time t1;
  Time t2;
  t1.inputTime("First");
  t2.inputTime("Second");
  
  //https://beginnersbook.com/2017/12/c-program-to-add-two-times/
  int seconds = t1.second+t2.second;
  int minute = t1.minute+t2.minute+(seconds/60);
  int hour = t1.hour+t2.hour+(minute/60);
  
  minute = minute%60;
  seconds = seconds%60;

  cout<<hour<<":"<<minute<<":"<<seconds;
}

int main() {
    addTwoTimes();
}
