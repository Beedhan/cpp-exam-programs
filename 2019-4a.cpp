//2019 4a
//? Written by Niraj 
#include <iostream>
using namespace std;

class customer
{
public:
    int id;
    string name;
    string address;
    int phone;

    void getdata()
    {
        cout << "Id: " << endl;
        cin >> id;
        cout << "Name: " << endl;
        cin >> name;
        cout << "Address: " << endl;
        cin >> address;
        cout << "Phone: " << endl;
        cin >> phone;
    }

    void display()
    {
        cout << "Id: " << id << " Name: " << name << " Address: " << address << " Phone: " << phone << endl;
    }
};

int main()
{
    int size = 3;

    customer *p = new customer[size];
    customer *ptr = p;
    for (int i = 0; i < size; i++)
    {
        p->getdata();
        p++;
    }

    for (int i; i < size; i++)
    {
        ptr->display();
        ptr++;
    }
}